function cacheFunction(callback){

    let cachedObject={};
    return function invokes(attr){
        if(cachedObject.hasOwnProperty(attr)){
            return cachedObject[attr];
        }
        else{
            cachedObject[attr] = callback(attr);
            console.log(cachedObject[attr]);
            return cachedObject;
            
        }
    }


}




module.exports = cacheFunction;


// at first we have to call the cacheFunction passing the callback method i.e cachefunction(cb);
// now on calling that function it will RETURN us the INVOKES FUNCTION. 
// now we have to pass one argument to the Invokes Function and that argument will be tracked by the CacheObject
// now invokes function has CallBack function inside it...which will help us in storing that passed argument to the CachedObject
// if that argument is allready present in the CachedObject then it should not invoke cb again to store that argument to the CachedObject 


