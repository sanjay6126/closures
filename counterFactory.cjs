// function counterFactory(){
//     let preCounter = 0;
//     function changeBy(val){ preCounter += val;}

//     return { increment(){ changeBy(1)} , decrement(){changeBy(-1)}}

// }


function counterFactory(){
    let preCounter = 5;
    function changeBy(val){ preCounter += val; return preCounter;}

    return { increment(){ return changeBy(1)} , decrement(){return changeBy(-1)}}

}

// let cf = counterFactory();
// console.log(cf.increment())
// console.log(cf.decrement())


module.exports = counterFactory;